//Напишите функцию, которая проверяет, содержит ли слайс хотя бы один элемент, удовлетворяющий заданному условию.

package main

import "fmt"

func main() {
	a := []int{45, 33, 4, 67, 56, 100}
	makeFilter := func(i int) bool { return i > 40 && i < 60 && i != 56 }
	a = filter(a, makeFilter)
	fmt.Println(a)
}

func filter(a []int, filt func(int) bool) []int {
	res := make([]int, 0, len(a))
	for _, el := range a {
		if filt(el) {
			res = append(res, el)
		}
	}
	return res
}
