//Напишите функцию, которая сортирует слайс строк в порядке возрастания.

package main

import (
	"fmt"
	"sort"
)

func main() {
	sl := []string{"sdf", "sdzxf", "sdf", "Sdf", "gtr", "Gtr", "r", "gtr"}
	fmt.Println(sl)

	//sort.Slice(sl, func(i, j int) bool {
	//	return sl[i] < sl[j]
	//})
	sort.Slice(sl, func(i, j int) bool {
		return len(sl[i]) < len(sl[j])
	})
	fmt.Println(sl)
}
