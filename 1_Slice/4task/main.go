//Напишите функцию, которая объединяет два слайса целых чисел и удаляет дубликаты.

package main

import "fmt"

func main() {
	sl1 := []int{1, 65, 4, 8, 7}
	sl2 := []int{1, 65, 65, 65, 41, 8, 73}
	fmt.Println(addSliceDelDubl(sl1, sl2))
}

func addSliceDelDubl(ar []int, ar2 []int) []int {
	res := append(ar, ar2...)
	for i, el := range res {
		for j := i + 1; j < len(res); j++ {
			if el == res[j] {
				res = append(res[:j], res[j+1:]...)
				j--

			}
		}
	}
	return res
}
