//Напишите функцию, которая удаляет из слайса все элементы, не удовлетворяющие заданному условию.

package main

import "fmt"

func main() {
	a := []int{34, 66, 45, 4, 65, 4}
	make_filter := func(i int) bool { return i%2 == 0 }
	//a = filterDel(a, make_filter)
	a = filter2(a, make_filter)
	fmt.Println(a)
}

//func filterDel(ar []int, f func(int) bool) (res []int) {
//	//res = make([]int, 0, len(ar))
//	for _, el := range ar {
//		if f(el) {
//			continue
//		} else {
//			res = append(res, el)
//		}
//	}
//	return
//}

func filter2(a []int, f func(int) bool) []int {
	for i := 0; i < len(a); i++ {
		if f(a[i]) {
			a = append(a[:i], a[i+1:]...)
			i--
		}
	}
	return a
}
