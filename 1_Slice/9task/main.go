//Напишите функцию, которая возвращает новый слайс, состоящий из элементов исходного слайса в обратном порядке.

package main

import (
	"fmt"
)

func main() {
	a := []int{6, 5, 3, 6, 77, 6767}
	Reverse(a)
	fmt.Println(a)
}

func Reverse[T any](a []T) []T {
	for i := 0; i < len(a)/2; i++ {
		a[i], a[len(a)-i-1] = a[len(a)-i-1], a[i]
	}
	return a
}
