//Напишите функцию, которая возвращает новый слайс, состоящий из первых N элементов исходного слайса.

package main

import "fmt"

func main() {
	a := []string{"sdf", "retqt", "sdfe", "weert"}
	a = newSlice(a, 2)
	fmt.Println(a)
}

func newSlice[T any](ar []T, i int) []T {
	res := ar[:i]
	return res
}
