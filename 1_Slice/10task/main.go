//Напишите функцию, которая удаляет из слайса все элементы, равные нулю.

package main

import "fmt"

func main() {
	r := []int{5, 9, 0, 7, 0, 67, 0}
	fmt.Println(dellZero(r))
}

func dellZero(s []int) []int {
	for i := 0; i < len(s); i++ {
		if s[i] == 0 {
			s = append(s[:i], s[i+1:]...)
			i--
		}
	}
	return s
}
