//Напишите функцию, которая находит максимальный элемент в слайсе целых чисел.

package main

import "fmt"

func main() {
	a := []int{5, 4, 3, 2, 1, 8, 77, 56}
	fmt.Println(max(a))
}

func max(ar []int) int {
	max := ar[0]
	for _, el := range ar {
		if el > max {
			max = el
		}
	}
	return max
}
