//Напишите функцию, которая возвращает новый слайс, состоящий только из четных чисел из исходного слайса.

package main

import "fmt"

func main() {
	a := []int{32, 4, 556, 34, 654, 5}
	even := func(i int) bool { return i%2 == 0 }
	fmt.Println(evenSlice(a, even))
}

func evenSlice(a []int, filter func(int) bool) []int {
	res := make([]int, 0, len(a))
	for _, el := range a {
		if filter(el) {
			res = append(res, el)
		}
	}
	return res
}
