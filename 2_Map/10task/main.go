//Напишите функцию, которая возвращает новый map, в котором значения исходного map умножены на заданный множитель.

package main

import "fmt"

func main() {
	m1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "sa": 15}
	fmt.Println(multMapValue(2, m1))
}

func multMapValue(n int, a map[string]int) map[string]int {
	res := make(map[string]int, len(a))
	for k, v := range a {
		res[k] = v * n
	}
	return res
}
