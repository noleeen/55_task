//Напишите функцию, которая удаляет из map все элементы, ключи которых удовлетворяют заданному условию.

package main

import (
	"fmt"
	"strings"
)

func main() {
	map1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "sa": 15}
	filter := func(i string) bool { return strings.HasSuffix(i, "a") }
	delForKey(map1, filter)
	fmt.Println(map1)
}

func delForKey(a map[string]int, fu func(string) bool) {
	for k := range a {
		if fu(k) {
			delete(a, k)
		}
	}
}
