//Напишите функцию, которая считает количество вхождений каждого элемента в слайс и возвращает результат
//в виде map[string]int, где ключ - это элемент, а значение - количество его вхождений.

package main

import "fmt"

func main() {
	st := []string{"sdf", "sdf", "sdf", "ert", "ert", "w", "45", "45", "sdf"}
	fmt.Println(countEntry(st))
}

func countEntry(a []string) map[string]int {
	entry := make(map[string]int, len(a))
	for _, el := range a {
		count := 0
		_, ok := entry[el]
		if ok {
			continue
		}
		for _, el2 := range a {
			if el == el2 {
				count++
			}
		}
		entry[el] = count
	}
	return entry
}
