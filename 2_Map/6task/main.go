//Напишите функцию, которая возвращает новый map, в котором ключи и значения исходного map переведены в нижний регистр.

package main

import (
	"fmt"
	"strings"
)

func main() {
	map1 := map[string]string{"wS": "s", "aSa": "RtR", "Q": "EW", "s": "df"}
	map2 := map[string]string{"wS": "s", "aSa": "RtR", "Q": "EW", "s": "df"}
	mapUpCase(map1)       //функция изменяет только значения в исходной мапе
	q := mapUpCase2(map2) //функция создаёт новую мапу изменяя и ключи и значения
	fmt.Println(map1)
	fmt.Println(q)
	fmt.Println(map2)
}

func mapUpCase(a map[string]string) {
	for k, v := range a {
		a[k] = strings.ToUpper(v)
	}
}

func mapUpCase2(a map[string]string) map[string]string {
	res := make(map[string]string, len(a))
	for k, v := range a {
		res[strings.ToUpper(k)] = strings.ToUpper(v)
	}
	return res
}
