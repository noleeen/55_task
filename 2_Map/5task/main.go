// Напишите функцию, которая возвращает новый map, в котором ключи и значения исходного map поменяны местами.

package main

import "fmt"

func main() {
	map1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "s": 15}
	mapRes := replaceKeyValue(map1)
	fmt.Println(mapRes)

}

func replaceKeyValue[V comparable, T comparable](a map[V]T) map[T]V {
	res := make(map[T]V, len(a))
	for k, v := range a {
		res[v] = k
	}
	return res
}
