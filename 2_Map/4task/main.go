//Напишите функцию, которая проверяет, содержит ли map значение, удовлетворяющее заданному условию.

package main

import "fmt"

func main() {
	map1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "s": 15}
	filter := func(i int) bool { return i > 100 }
	filter2 := func(i int) bool { return i == 45 }
	fmt.Println(mapExist(map1, filter))
	fmt.Println(mapExist(map1, filter2))
}

func mapExist(a map[string]int, fu func(int) bool) bool {
	for k := range a {
		if fu(a[k]) {
			return true
		}
	}
	return false
}
