//Напишите функцию, которая возвращает новый map, содержащий только те элементы, ключи которых удовлетворяют заданному условию.

package main

import (
	"fmt"
	"strings"
)

func main() {
	map1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "s": 15}
	filter1 := func(i string) bool { return len(i) > 1 }
	filter2 := func(i string) bool { return strings.HasPrefix(i, "s") }
	fmt.Println(mapFilter(map1, filter1))
	fmt.Println(mapFilter(map1, filter2))
}

func mapFilter(m map[string]int, f func(string) bool) map[string]int {
	res := make(map[string]int, len(m))
	for k, v := range m {
		if f(k) {
			res[k] = v
		}
	}
	return res
}
