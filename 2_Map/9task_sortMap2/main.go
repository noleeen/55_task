//Напишите функцию, которая сортирует map по значениям в порядке возрастания и возвращает новый map.

package main

import (
	"fmt"
	"sort"
)

type key_value struct {
	key   string
	value int
}

func main() {
	m1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "sa": 15}
	fmt.Println(sortMapValue(m1))
}

func sortMapValue(a map[string]int) []key_value {
	newStruct := make([]key_value, len(a))
	i := 0
	for k, v := range a {
		newStruct[i] = key_value{k, v}
		i++
	}
	sort.Slice(newStruct, func(i, j int) bool {
		return newStruct[i].value < newStruct[j].value
	})
	return newStruct
}
