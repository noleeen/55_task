//Напишите функцию, которая возвращает сумму всех значений в map.

package main

import "fmt"

func main() {
	map1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "s": 15}
	fmt.Println(sumMapValue(map1))
}

func sumMapValue(a map[string]int) int {
	sum := 0
	for _, v := range a {
		sum += v
	}
	return sum
}
