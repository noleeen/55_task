//Напишите функцию, которая объединяет два map[string]int и возвращает новый map, содержащий значения из обоих
//исходных map, если ключи не дублируются. Если ключи дублируются, то значение должно быть суммой значений для каждого ключа.
package main

import "fmt"

func main() {
	map1 := map[string]int{"s": 45, "w": 4, "a": 5, "q": 45, "r": 15}
	map2 := map[string]int{"sw": 1, "w": 1, "a": 5, "qe": 0, "r": 15}

	//sumMap(map1, map2) // эта функция изменяет вторую мапу

	q := sumMap2(map1, map2) // эта функция создаёт новую не мемняя входящие
	fmt.Println(map2)        //
	fmt.Println(q)           //
}

//func sumMap(m1, m2 map[string]int) map[string]int {
//	for k, v := range m1 {
//		m2[k] += v
//	}
//	return m2
//}

func sumMap2(m1, m2 map[string]int) map[string]int {
	res := make(map[string]int, len(m1))
	for k, v := range m1 {
		res[k] += v
	}
	for k, v := range m2 {
		res[k] += v
	}
	return res
}
