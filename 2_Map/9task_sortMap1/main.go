//Напишите функцию, которая сортирует map по значениям в порядке возрастания и возвращает новый map.

package main

import (
	"fmt"
	"sort"
)

type In interface {
	Less(i, j int) bool
	Len() int
	Swap(i, j int)
}

type key_val struct {
	key   string
	value int
}

type sorted_struct []key_val

func (s sorted_struct) Less(i, j int) bool { return s[i].value < s[j].value }
func (s sorted_struct) Len() int           { return len(s) }
func (s sorted_struct) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func main() {
	map1 := map[string]int{"sf": 45, "w": 4, "asa": 5, "q": 45, "sa": 15}
	fmt.Println(sortMapVal(map1))
}

func sortMapVal(map1 map[string]int) []key_val {
	newStruct := make(sorted_struct, len(map1))

	i := 0
	for k, v := range map1 {
		newStruct[i] = key_val{k, v}
		i++
	}
	sort.Sort(newStruct)
	return newStruct
}
