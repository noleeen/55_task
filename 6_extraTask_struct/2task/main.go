//Создайте структуру Animal, которая содержит поля имя, вид и список характеристик.
//Каждая характеристика представляется в виде структуры Characteristic, которая содержит поле название и значение.
//Используйте мапу для хранения списка животных. Напишите функцию для вывода всех животных и их характеристик.

package main

import "fmt"

type Animal struct {
	name       string
	species    string
	properties []Properties
}

type Properties struct {
	title string
	value string
}

func main() {
	characteristicsLion := []Properties{
		{"weight", "15"},
		{"speed", "40"},
	}
	characteristicsBear := []Properties{
		{"weight", "120"},
		{"speed", "25"},
	}

	animals := []Animal{
		{
			"lion",
			"cat",
			characteristicsLion,
		},
		{
			"bear",
			"unknown",
			characteristicsBear,
		},
	}

	animalsMap := make(map[string]Animal, len(animals))
	for i := 0; i < len(animals); i++ {
		animalsMap[animals[i].name] = animals[i]
	}
	animalInfo(animalsMap)
}

func animalInfo(a map[string]Animal) {
	for k, v := range a {
		fmt.Printf("\n %s\ncharacteristics:", k)
		for i := range v.properties {
			fmt.Printf("\n%s: %s", v.properties[i].title, v.properties[i].value)
		}
	}
}
