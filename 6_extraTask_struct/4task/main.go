////Создайте интерфейс Fighter, который содержит методы Attack() и Defend().
////Создайте структуру Knight, которая реализует интерфейс Fighter и содержит поле силы атаки.
////Создайте структуру Mage, которая также реализует интерфейс Fighter и содержит поле силы магии.
////Создайте функцию, которая принимает объект типа Fighter и вызывает его методы Attack() и Defend().
package main

//
//import "fmt"
//
//type Fighter interface {
//	Attack(a Knight, b Magic, i int)
//	Defend()
//}
//
//type Knight struct {
//	title       string
//	powerAttack int
//	lives       int
//	defend      int
//}
//
//func (k *Knight) Attack(a Knight, b Magic, i int) {
//	if i == 1 {
//		a.lives -= k.powerAttack
//	} else if i == 2 {
//		b.lives -= k.powerAttack
//	} else {
//		fmt.Println("attack was miss")
//	}
//}
//func (k *Knight) Defend() {
//	k.lives += k.defend
//	k.powerAttack -= k.defend
//}
//func (k *Knight) DefendEnd() {
//	k.lives -= k.defend
//	k.powerAttack += k.defend
//}
//
//type Mage struct {
//	title      string
//	powerMagic int
//	lives      int
//	defend     int
//}
//type Magic Mage
//
//func (k Mage) Attack(a Knight, b Magic, i int) {
//	if i == 1 {
//		a.lives -= k.powerMagic
//	} else if i == 2 {
//		b.lives -= k.powerMagic
//	} else {
//		fmt.Println("attack was miss")
//	}
//}
//func (k Mage) Defend() {
//	k.lives += k.defend
//	k.powerMagic -= k.defend
//}
//func (k Mage) DefendEnd() {
//	k.lives -= k.defend
//	k.powerMagic += k.defend
//}
//
//func main() {
//	Gendelf := Magic{"Magik", 40, 100, 100}
//	Fredrick := Knight{"R", 60, 120, 30}
//	fmt.Println(Gendelf, Fredrick)
//}
//
////func attackForMage(a Fighter, b Fighter){
////	a.Attack(a, b, 2)
////}
