//Создайте интерфейс Transporter, который содержит метод Transport() для перемещения объекта в место назначения.
//Создайте структуру Car, которая содержит поле название и метод Transport(), который выводит сообщение о перемещении
//машины в место назначения. Создайте структуру Plane, которая также содержит поле название и метод Transport(),
//который выводит сообщение о перемещении самолета в место назначения.
//Используйте мапу для хранения списка транспортных средств.
//Напишите функцию для перемещения всех транспортных средств в место назначения.

package main

import "fmt"

type Transporter interface {
	Transport()
}

type Car struct {
	nameCar string
}

func (c Car) Transport() {
	fmt.Println("Car", c.nameCar, "arrived to destinations")
}

type Plane struct {
	namePlane string
}

func (p Plane) Transport() {
	fmt.Println("Plane", p.namePlane, "arrived to destinations")
}

func main() {
	listT := [][]string{
		{"car", "Honda"},
		{"car", "Renaught"},
		{"car", "Kia"},
		{"plane", "Rob2"},
		{"plane", "Airsun"},
	}
	var transport []Transporter

	for i := range listT {
		if listT[i][0] == "car" {
			transport = append(transport, Car{listT[i][1]})
		} else if listT[i][0] == "plane" {
			transport = append(transport, Plane{listT[i][1]})
		}
	}
	fmt.Println(transport)
	goToDestinations(transport)
}

func goToDestinations(a []Transporter) {
	for i := range a {
		a[i].Transport()
	}
}
