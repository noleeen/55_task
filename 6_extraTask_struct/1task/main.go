//Создайте структуру Student, которая содержит поля имя, возраст и список курсов, которые студент проходит.
//Каждый курс представляется в виде структуры Course, которая содержит поля название курса, описание и оценку.
//Используйте мапу для хранения списка студентов. Напишите функцию для вывода всех студентов и их курсов с оценками.

package main

import "fmt"

type Course struct {
	title string
	info  string
	eval  int
}

type Students struct {
	name    string
	age     int
	courses []Course
}

func main() {
	courseIvan := []Course{
		{
			"math",
			"part2",
			9,
		},
		{
			"golang",
			"interfaces",
			8,
		},
	}
	courseKate := []Course{
		{
			"cooking",
			"baking",
			9,
		},
	}
	st := []Students{
		{
			name:    "Ivan",
			age:     25,
			courses: courseIvan,
		},
		{
			name:    "Kate",
			age:     27,
			courses: courseKate,
		},
	}

	stMap := make(map[string][]Course, len(st))
	for i := 0; i < len(st); i++ {
		stMap[st[i].name] = st[i].courses
	}
	fmt.Println(stMap)
	studentsInfo(stMap)
}

func studentsInfo(a map[string][]Course) {
	for k, v := range a {
		for i := range v {
			fmt.Printf("\nname: %s\ncourse: %s\neval: %d\n", k, v[i].title, v[i].eval)
		}
	}
}
