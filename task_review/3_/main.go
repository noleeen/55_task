package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// User создай структуру User.
type User struct {
	name   string
	age    int
	income int
}

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	age := rand.Intn(53) + 18
	// используй rand.Intn()
	return age
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {

	income := rand.Intn(500000) // используй rand.Intn()
	return income
}

// generateFullName сгенерируй полное имя. например "John Doe".
func generateFullName() string {
	name := []string{"Bob", "Steave", "Bill", "Adam", "Barbara"}
	name2 := []string{"Rob", "Silt", "Lockl", "Dammer", "Bardak"}

	// создай слайс с именами и слайс с фамилиями.
	// используй rand.Intn() для выбора случайного имени и фамилии.

	return fmt.Sprint(name[rand.Intn(len(name))], name2[rand.Intn(len(name2))])
}

func main() {
	users := make([]User, 0, 100)
	for i := 0; i < 101; i++ {
		temp := User{generateFullName(), generateAge(), generateIncome()}
		users = append(users, temp)
	}

	fmt.Println(users)
	// Сгенерируй 1000 пользователей и заполни ими слайс users.

	// Выведи средний возраст пользователей.
	var averageAge, averageIncome, sumAge, sumIncome float64
	for i := range users {
		sumAge += float64(users[i].age)
	}
	for i := range users {
		sumIncome += float64(users[i].income)
	}
	averageAge = sumAge / float64(len(users))
	averageIncome = sumIncome / float64(len(users))
	fmt.Println(averageAge, averageIncome)

	// Выведи средний доход пользователей.
	for i := range users {
		if averageIncome < float64(users[i].income) {
			fmt.Println(users[i].name, users[i].age, users[i].income)
		}
	}
	// Выведи количество пользователей, чей доход превышает средний доход.

}
