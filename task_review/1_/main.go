package main

import (
	"fmt"
	"math/rand"
	"sort"
)

type Reverser interface {
	Reverse()
}

type Sorter interface {
	Sort()
}

type SorterReverser interface {
	Reverse()
	Sort()
}

type Numbers []int

func (n Numbers) Sort() { // функция сортировки массива, использовать библиотеку function_in_function
	sort.Ints(n)
}

func (n Numbers) Reverse() { // функция переворачивания массива, перевернуть используя метод a, b = b, a
	for i := 0; i < len(n)/2; i++ {
		n[i], n[len(n)-1-i] = n[len(n)-1-i], n[i]
	}
}

func main() {
	var numbers Numbers
	numbers = AddNumbers(numbers) // исправить чтобы добавляло значения
	SortReverse(numbers)          // исправить чтобы сортировало и переворачивала массив
	fmt.Println(numbers)
}

func AddNumbers(s Numbers) Numbers {
	for i := 0; i < 100; i++ {
		s = append(s, rand.Intn(100))
	}
	return s
}

func SortReverse(sr SorterReverser) {
	sr.Sort()
	sr.Reverse()

}
