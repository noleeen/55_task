//Напишите функцию, которая принимает на вход два значения любого типа и возвращает true, если они равны,
//и false в противном случае. Функция должна использовать интерфейсы для работы с различными типами данных.

package main

import "fmt"

type In interface {
	//Len() int
}

type Human struct {
	name string
}

type Ingeneer Human

func (i Ingeneer) Len() int { return len(i.name) }

type Driver Human

func (d Driver) Len() int { return len(d.name) }
func (d Driver) Hello()   { fmt.Println("Hello,", d.name) }

func main() {
	us1 := Driver{"Steve"}
	us2 := Ingeneer{"John"}
	us3 := Ingeneer{"Steve"}
	fmt.Printf("us1 type: %T\n us2 type: %T\n", us1, us2)
	if us1.name == us2.name {
		fmt.Println()
	}
	fmt.Println(comparisonTypes(us1, us3))
	d := 5
	v := "5"
	fmt.Println(comparisonTypes(v, d))
}

func comparisonTypes(a In, b In) bool {

	return a == b
}
