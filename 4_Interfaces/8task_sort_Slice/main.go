//сортировка структуры с пом пустого интерфекйса

package main

import (
	"fmt"
	"sort"
)

type Human struct {
	name string
	sex  string
	age  int
}

func main() {

	g5 := []Human{
		{
			"S",
			"M",
			8,
		},
		{
			"e",
			"m",
			8,
		},
	}
	g5 = append(g5, Human{"dad", "f", 10})
	g5 = append(g5, Human{"Dad", "F", 11})
	fmt.Printf("%T : %v\n", g5, g5)
	SortForAge(g5)
	fmt.Println(g5)
	SortForName(g5)
	fmt.Println(g5)
	SortForSex(g5)
	fmt.Println(g5)

}

func SortForAge(a interface{}) {
	t, ok := a.([]Human)
	if ok {
		sort.Slice(a, func(i, j int) bool { return t[i].age < t[j].age })
	} else {
		fmt.Println("cannot be sorted. error type")
	}
}
func SortForName(a interface{}) {
	t, ok := a.([]Human)
	if ok {
		sort.Slice(a, func(i, j int) bool { return t[i].name < t[j].name })
	} else {
		fmt.Println("cannot be sorted. error type")
	}
}
func SortForSex(a interface{}) {
	t, ok := a.([]Human)
	if ok {
		sort.Slice(a, func(i, j int) bool { return t[i].sex < t[j].sex })
	} else {
		fmt.Println("cannot be sorted. error type")
	}
}
