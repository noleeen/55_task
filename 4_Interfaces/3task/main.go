//Напишите интерфейс Shape, который определяет методы для вычисления площади и периметра фигуры.

package main

import "fmt"

type Shape interface {
	Area() float32
	Perimetr() float32
}

type square struct {
	size int
}

func (s square) Area() float32     { return float32(s.size) * float32(s.size) }
func (s square) Perimetr() float32 { return float32(s.size) * 4 }

func main() {
	sq1 := square{5}
	sq2 := square{2}

	fmt.Println(sq1.Perimetr())
	fmt.Println(sq1.Area())
	fmt.Println(sq2.Perimetr())
	fmt.Println(sq2.Area())
}
