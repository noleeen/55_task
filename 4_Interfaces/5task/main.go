//Напишите функцию, которая принимает на вход интерфейс Shape и выводит его площадь и периметр.

package main

import (
	"fmt"
	"math"
)

type Shape interface {
	Area() float64
	Perimetr() float64
}

type Circle struct {
	Radius int
}

func (c Circle) Area() float64     { return math.Pow(float64(c.Radius), 2) * math.Pi }
func (c Circle) Perimetr() float64 { return 2 * float64(c.Radius) * math.Pi }

func main() {
	cirle1 := Circle{7}
	AreaPerimetr(cirle1)
}

func AreaPerimetr(a Shape) {
	fmt.Println(a.Area())
	fmt.Println(a.Perimetr())
}
