//Напишите функцию, которая принимает на вход любой тип данных и выводит его значения в строковом формате.

package main

import "fmt"

func main() {
	p := 23
	outputStr(p)

}

func outputStr(a interface{}) {
	fmt.Println(a)
}
