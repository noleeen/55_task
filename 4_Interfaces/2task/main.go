//Напишите функцию, которая принимает на вход любой тип данных и возвращает true, если это строка, и false в противном случае.

package main

import "fmt"

func main() {
	i := 7
	s := ""
	fmt.Println(StringOrNot(i))
	fmt.Println(StringOrNot(s))
}

func StringOrNot(a interface{}) bool {
	switch a.(type) {
	case string:
		return true
	default:
		return false
	}
}
