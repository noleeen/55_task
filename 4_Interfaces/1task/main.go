//Напишите функцию, которая принимает на вход любой тип данных и выводит его тип.

package main

import "fmt"

func main() {
	g := 4
	f := [5]string{}
	o := make([]string, 2)
	d := "g,2)"
	typeData(g)
	typeData(f)
	typeData(o)
	typeData(d)
}

func typeData(a interface{}) {
	fmt.Printf("%T\n", a)
}
