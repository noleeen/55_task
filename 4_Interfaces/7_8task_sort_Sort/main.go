//Напишите интерфейс Sortable, который определяет метод Len() для получения длины списка,
//метод Swap() для обмена элементами и метод Less() для сравнения двух элементов списка.

//Напишите функцию, которая сортирует список элементов любого типа данных, реализующих интерфейс Sortable.

package main

import (
	"fmt"
	"sort"
)

type Sortable interface {
	Less(i, j int) bool
	Len() int
	Swap(i, j int)
}

type Human struct {
	name string
	sex  string
	age  int
}

type Children []Human

func (ch Children) Less(i, j int) bool { return ch[i].name < ch[j].name }
func (ch Children) Len() int           { return len(ch) }
func (ch Children) Swap(i, j int)      { ch[i], ch[j] = ch[j], ch[i] }

func main() {
	//var g4 Children
	g4 := Children{{"df", "d", 5}}
	g4 = append(g4, Human{"a", "d", 6})
	g4 = append(g4, Human{"d", "f", 11})
	g4 = append(g4, Human{"dad", "f", 10})

	//g5 := []Human {
	//	{
	//		"S",
	//		"m",
	//		8,
	//	},
	//	{
	//		"S",
	//		"m",
	//		8,
	//	},
	//}
	fmt.Printf("%T : %v\n", g4, g4)
	SortAllTypes(g4)

	fmt.Println(g4)
}

func SortAllTypes(in Sortable) {
	sort.Sort(in)
}
