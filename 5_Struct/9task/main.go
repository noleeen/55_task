//Напишите структуру BankAccount, которая содержит поля balance и owner. Напишите методы для снятия и внесения денег со счета.

package main

import "fmt"

type BancAccount struct {
	owner   string
	balance int
}

func (b *BancAccount) addB(i int) { b.balance += i }

func (b *BancAccount) diffB(i int) { b.balance -= i }

func main() {
	users := []BancAccount{
		{
			"Ivan",
			1500,
		},
		{
			"Kate",
			150,
		},
	}
	addBalance(users, "Kate", 120)
	minusBalance(users, "Ivan", 120)
	fmt.Println(users)
}

func addBalance(u []BancAccount, name string, num int) {
	for i, el := range u {
		if el.owner == name {
			u[i].addB(num)
		}
	}
}
func minusBalance(u []BancAccount, name string, num int) {
	for i, el := range u {
		if el.owner == name {
			u[i].diffB(num)
		}
	}
}
