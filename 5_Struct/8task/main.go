//Напишите структуру Point, которая содержит поля x и y.
//Напишите метод, который возвращает расстояние между двумя точками.

package main

import (
	"fmt"
	"math"
)

type Point struct {
	x int
	y int
}

type points []Point

func (p points) distancePoints(a Point, b Point) float64 {
	distance := math.Sqrt(math.Pow(float64(a.x-b.x), 2) + math.Pow(float64(a.y-b.y), 2))
	return distance
}

func main() {
	a := points{
		{
			2,
			4,
		},
		{
			3,
			7,
		},
	}
	fmt.Println(a.distancePoints(a[0], a[1]))
}
