//Напишите структуру Employee, которая содержит поля name, salary и position.
//Напишите метод, который повышает зарплату сотрудника на указанную сумму.

package main

import "fmt"

type Employee struct {
	salary   int
	position string
}

func (e *Employee) addSalary(i int) { e.salary = e.salary + i }

func main() {
	us1 := []Employee{
		{
			120,
			"manager",
		},
		{
			200,
			"director",
		},
	}
	us1[0].addSalary(30)
	fmt.Println(us1)
}
