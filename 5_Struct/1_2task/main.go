//Напишите структуру Rectangle, которая содержит поля width и height.
//Напишите методы для вычисления периметра и площади прямоугольника.

//Напишите структуру Circle, которая содержит поле radius.
//Напишите методы для вычисления диаметра, длины окружности и площади круга.

package main

import (
	"fmt"
	"math"
)

type Rectangle struct {
	width  int
	height int
}

func (r Rectangle) Area() int     { return r.width * r.height }
func (r Rectangle) Perimetr() int { return (r.width + r.height) * 2 }

type Circle struct {
	radius float64
}

func (c Circle) Diametr() float64  { return c.radius * 2 }
func (c Circle) Area() float64     { return math.Pi * math.Pow(c.radius, 2) }
func (c Circle) Perimetr() float64 { return 2 * math.Pi * c.radius }

func main() {
	rect1 := []Rectangle{{5, 2}, {4, 3}}
	circle := Circle{11}

	fmt.Println(rect1[0].Area(), rect1[0].Perimetr(), rect1[1].Area(), rect1[1].Perimetr())
	fmt.Printf("диаметр круга: %.0f\nплощадь круга: %.3f\nдлина окружности: %.2f", circle.Diametr(), circle.Area(), circle.Perimetr())

}
